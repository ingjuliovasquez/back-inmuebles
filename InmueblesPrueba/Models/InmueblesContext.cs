﻿using Microsoft.EntityFrameworkCore;

namespace InmueblesPrueba.Models
{
    public class InmueblesContext: DbContext
    {
        public InmueblesContext(DbContextOptions<InmueblesContext> options)
            : base(options) 
        {
            
        }
        public DbSet<Inmueble> Inmuebles { get; set; }
    }
}
